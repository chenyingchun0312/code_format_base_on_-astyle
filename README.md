# code_format_AStyle

#### 介绍
基于AStyle，
windows平台，代码格式化脚本。目前仅支持C、C++语言。

目前支持以下操作：

1. 双击执行转换当前目录及子目录中文件  
2. 拖拽任意路径的单个文件  
3. 拖拽任意路径的目录  
4. 在 .C/.H 上右键选择格式化
5. 

**注意：**
- **批处理中关闭了文件备份，如果需要可以自行修改**

- **默认使用google风格格式化**

  

#### 安装教程

1.  [点击下载  AStyle](https://fossies.org/windows/misc/AStyle_3.1_windows.zip/index_tp.html)
2.  [点击下载 右键菜单管理工具]([ContextMenuManager 发行版 - Gitee.com](https://gitee.com/BluePointLilac/ContextMenuManager/releases))
3.  解压 AStyle 后放在任意地址，请记住它的路径
4.  修改`auto_format_AStyle.bat`脚本中的  ***set AsPath="D:\AStyle\bin\astyle.exe"*** 为自己的上述路径

 ![image-20210811180701757](https://pic.imgdb.cn/item/6113a1475132923bf88cf10d.png)

5. 右键管理工具有很多，可自行选择

    ![image-20210811181440708](https://pic.imgdb.cn/item/6113a3125132923bf89144ab.png)

 ![image-20210811181538037](https://pic.imgdb.cn/item/6113a34b5132923bf891cf7b.png)

![image-20210811181634978](https://pic.imgdb.cn/item/6113a3845132923bf8925245.png)



**至此，已经完成了所有的设置。选择一个任意 .c 文件，进行测试：**

 ![image-20210811182408863](https://pic.imgdb.cn/item/6113a54a5132923bf8966003.png)



#### 使用说明

1.  
2.  xxxx
3.  xxxx

#### 参与贡献

1.  
2.  
3.  
4.  


#### 特技

