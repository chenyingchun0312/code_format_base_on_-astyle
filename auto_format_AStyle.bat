@echo off
:: Created     Author       Note
:: 2020-8-13   Internet     first version
:: 2020-12-6   svchao       修改支持单/多文件拖放
:: 2021-8-11   svchao       修改添加右键支持

:: 
:: -A14  K&R 风格.
:: -s4  tab = 4spaces.
:: -n   不备份.
:: -c   Tab转为空格.
:: -o   不要打断一行的语句
:: -j   格局样式, 给if for while 添加 { 和 } 
:: -k3  将指针或引用运算符 靠右和变量靠近.
:: -w    宏定义换行后缩进
:: -xC   代码最大长度,超过则在 [, &] 等处换行
:: -Q    仅显示已格式化的文件.
:: -Z    不要修改文件时间
:: -v    详细显示模式, 比如发行号, 日期, 文件位置等
:: -xb   例如从if() 后断开语句,换行.
:: -W3   &靠近变量名. 如  char &foo.
:: -H    在if for while 后面插入一个空格 
:: -U    删除if for while 中间的不必要的空格.
:: -xg   在if(,)  逗号后插入空格.
:: -p    在运算符周围插入空格.
:: -f    在标题块(if / for /while)周围加空行.
:: -m    设置当标题由多行构成时的最小缩进量.
:: -Y    从第一行缩进注释.
:: -L    向标签添加额外的缩进, 以使他们比当前少一个缩进.
:: -xt#  设置以开始括号.
:: -xV   将do while 中的while 放到}后面.
::--------------------------------------------------------------------  
@echo off
color 2e
title C/C++代码格式化工具  
echo 文件支持类型c,h,cpp 
echo 1双击执行转换当前目录及子目录中文件  
echo 2拖拽任意路径的单个文件  
echo 3拖拽任意路径的目录  
echo 4右键选择格式化
echo.  
  
::这个必须要打开
setlocal enabledelayedexpansion


echo. 
::文件全名为: %~nx1, 文件名为： %~n1, 扩展名为： %~x1
::读取文件名一定要用 " " 括起来, 否则无效!!!
::set "filename=%~nx1"   
cd /d "%~dp0"  
set pats=%~dp1  

::首次使用,请修改下面的AsPath的路径!
set AsPath="D:\AStyle\bin\astyle.exe"  

set formatCMD=%AsPath% -A14 -s4 -n -S -Y -c -o -j -k3 -w -xC100 -Q -Z -xb -y -W3 -H -U -xg -p -f -m0 -L -xt1 -xV -v "!filename!"  

if /i "%~1"==""            (goto doubleClick)  
:: 'if exist' 判断目录是否存在
:: %~1：当参数以引号开头时，%~1会自动将引号删除, %1则不会.
if exist "%~1\"            (goto dir)  


:: /i 忽略大小写

for %%A in (%*) DO (set nl=1
	set "filename=%%A"
 	if /i "%%~XA"==".c" (%AsPath% -A14 -s4 -n -S -Y -c -o -j -k3 -w -xC100 -Q -Z -xb -y -W3 -H -U -xg -p -f -m0 -L -xt1 -xV -v "!filename!"
         set nl=0) 
        if /i "%%~XA"==".h" (%AsPath% -A14 -s4 -n -S -Y -c -o -j -k3 -w -xC100 -Q -Z -xb -y -W3 -H -U -xg -p -f -m0 -L -xt1 -xV -v "!filename!"
         set nl=0) 
        if /i "%%~XA"==".cpp" (%AsPath% -A14 -s4 -n -S -Y -c -o -j -k3 -w -xC100 -Q -Z -xb -y -W3 -H -U -xg -p -f -m0 -L -xt1 -xV -v "!filename!"
         set nl=0) 
          if "!nl!"=="1" (cls
	    ECHO --不支持的文件格式   [.c .h支持的]!
		ECHO.
	)
) 
pause
exit


::cls 清除屏幕
cls     
ECHO %filename%  
ECHO 是无效的后缀,当前支持的后缀类型是c,h ,要支持其他类型请修改参数  
pause  
exit  

::2020-12-6 这里暂时就用不到了
:file  
echo --------------------singleFile mode----------------------  
ECHO 您输入的文件是:
ECHO %filename%  
ECHO.
%AsPath% -A14 -s4 -n -S -Y -c -o -j -k3 -w -xC100 -Q -Z -xb -y -W3 -H -U -xg -p -f -m0 -L -xt1 -xV -v "%filename%"  
::上句中的参数按需修改  
  
REM 删除所有的备份文件  
REM del *.pre /s /q  
pause  
exit  

:dir  
echo ---------------------dir mode-----------------------------  
for /r "%~nx1" %%f in (*.cpp;*.c;*.h) do %AsPath% -A14 -s4 -n -S -Y -n -c -o -j -k3 -w -xC100 -Q -Z -xb -y -W3 -H -U -xg -p -f -m0 -L -xt1 -xV  "%%f"  
REM 删除所有的备份文件  
REM for /r "%~nx1" %%a in (*.pre) do del "%%a"  
pause  
exit  
:doubleClick  
echo -------------------doubleClick mode--------------------------  
for /r . %%f in (*.cpp;*.c;*.h) do %AsPath% -A14 -s4 -n -S -Y -n -c -o -j -k3 -w -xC100 -Q -Z -xb -y -W3 -H -U -xg -p -f -m0 -L -xt1 -xV "%%f"  
REM 删除所有的备份文件  
REM del *.pre /s /q  
pause  
exit 